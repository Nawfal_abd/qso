package fr.jrdf.qso.qso.config;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
@ComponentScan("fr.jrdf.qso.qso")
public class WebConfig implements WebMvcConfigurer {

	@Bean
	public CommonsMultipartResolver multipartResolver() {

		final CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		return resolver;
	}

	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new MappingJackson2HttpMessageConverter());
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
