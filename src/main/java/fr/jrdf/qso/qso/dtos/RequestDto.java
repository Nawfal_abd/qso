package fr.jrdf.qso.qso.dtos;

import fr.jrdf.qso.qso.utils.Env;

public class RequestDto {

	private int id;

	private String firstName;

	private String lastName;

	private String picture;
	// @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	// private LocalDate dateOfEntry;

	private String entrepriseName;

	private String entrepriseAddress;

	public RequestDto() {
		super();
	}

	public RequestDto(String firstName, String lastName, String picture, String entrepriseName,
			String entrepriseAddress) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.picture = picture;
		// this.dateOfEntry = dateOfEntry;
		this.entrepriseName = entrepriseName;
		this.entrepriseAddress = entrepriseAddress;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPicture() {
		return Env.getUrlImages() + picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	// public LocalDate getDateOfEntry() {
	// return dateOfEntry;
	// }
	//
	// public void setDateOfEntry(LocalDate dateOfEntry) {
	// this.dateOfEntry = dateOfEntry;
	// }

	public String getEntrepriseName() {
		return entrepriseName;
	}

	public void setEntrepriseName(String entrepriseName) {
		this.entrepriseName = entrepriseName;
	}

	public String getEntrepriseAddress() {
		return entrepriseAddress;
	}

	public void setEntrepriseAddress(String entrepriseAddress) {
		this.entrepriseAddress = entrepriseAddress;
	}

}
