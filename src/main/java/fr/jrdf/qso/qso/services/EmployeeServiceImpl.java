package fr.jrdf.qso.qso.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.jrdf.qso.qso.dtos.RequestDto;
import fr.jrdf.qso.qso.entities.Employee;
import fr.jrdf.qso.qso.entities.Entreprise;
import fr.jrdf.qso.qso.entities.User;
import fr.jrdf.qso.qso.repositories.EmployeeRepository;
import fr.jrdf.qso.qso.utils.ImageHandler;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

	private final EmployeeRepository employeeRepository;

	public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;

	}

	public void addEmployee(RequestDto request, MultipartFile image) throws IOException {

		final User user = new User();
		user.setFirstName(request.getFirstName());
		user.setLastName(request.getLastName());
		user.setPicture(ImageHandler.uploadFile(image));

		final Entreprise entreprise = new Entreprise();
		entreprise.setName(request.getEntrepriseName());
		entreprise.setAddress(request.getEntrepriseAddress());

		final Employee employee = new Employee();
		// employee.setDateOfEntry(request.getDateOfEntry());
		employee.setEntreprise(entreprise);
		employee.setUser(user);

		employeeRepository.save(employee);
	}

	public List<RequestDto> getEmployees() {
		final List<Employee> employees = employeeRepository.findAll();
		final List<RequestDto> requestList = new ArrayList<RequestDto>();

		for (final Employee employee : employees) {
			final RequestDto requestDto = new RequestDto();
			requestDto.setId(employee.getId());
			// requestDto.setDateOfEntry(employee.getDateOfEntry());
			requestDto.setEntrepriseAddress(employee.getEntreprise().getAddress());
			requestDto.setFirstName(employee.getUser().getFirstName());
			requestDto.setLastName(employee.getUser().getLastName());
			requestDto.setPicture(employee.getUser().getPicture());
			requestDto.setEntrepriseName(employee.getEntreprise().getName());
			requestList.add(requestDto);
		}

		return requestList;
	}

	public List<RequestDto> getEmployeesBySearch(String searchKey) {

		final List<Employee> employees = employeeRepository.FindAllByLastNameAndFirstNameAndEntrprise(searchKey);
		final List<RequestDto> requestList = new ArrayList<RequestDto>();

		for (final Employee employee : employees) {
			final RequestDto requestDto = new RequestDto();
			requestDto.setId(employee.getId());
			// requestDto.setDateOfEntry(employee.getDateOfEntry());
			requestDto.setEntrepriseAddress(employee.getEntreprise().getAddress());
			requestDto.setFirstName(employee.getUser().getFirstName());
			requestDto.setLastName(employee.getUser().getLastName());
			requestDto.setPicture(employee.getUser().getPicture());
			requestDto.setEntrepriseName(employee.getEntreprise().getName());
			requestList.add(requestDto);
		}

		return requestList;
	}

	public void deleteEmployee(int id) {
		employeeRepository.deleteById(id);

	}

	public void editEmployee(RequestDto request, MultipartFile image) throws IOException {
		final User user = new User();
		user.setFirstName(request.getFirstName());
		user.setLastName(request.getLastName());
		user.setPicture(ImageHandler.uploadFile(image));

		final Entreprise entreprise = new Entreprise();
		entreprise.setName(request.getEntrepriseName());
		entreprise.setAddress(request.getEntrepriseAddress());

		final Employee employee = new Employee();
		// employee.setDateOfEntry(request.getDateOfEntry());
		employee.setId(request.getId());
		employee.setEntreprise(entreprise);
		employee.setUser(user);

		employeeRepository.save(employee);

	}

}
