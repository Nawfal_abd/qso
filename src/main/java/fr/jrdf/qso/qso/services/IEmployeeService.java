package fr.jrdf.qso.qso.services;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import fr.jrdf.qso.qso.dtos.RequestDto;

public interface IEmployeeService {

	void addEmployee(RequestDto requestDto, MultipartFile image) throws IOException;

	List<RequestDto> getEmployees();

	List<RequestDto> getEmployeesBySearch(String searchKey);

	void deleteEmployee(int id);

	void editEmployee(RequestDto request, MultipartFile image) throws IOException;

}
