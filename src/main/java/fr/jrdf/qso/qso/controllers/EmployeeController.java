package fr.jrdf.qso.qso.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.jrdf.qso.qso.dtos.RequestDto;
import fr.jrdf.qso.qso.services.IEmployeeService;

@CrossOrigin("http://localhost:4200")
@RequestMapping("/employee")
@RestController
public class EmployeeController {

	private final IEmployeeService employeeService;

	public EmployeeController(IEmployeeService employeeService) {
		this.employeeService = employeeService;

	}

	@GetMapping("/hello")
	public String hello() {

		return "ss";
	}

	@DeleteMapping("/{id}")
	public String deleteEmployee(@PathVariable int id) {
		employeeService.deleteEmployee(id);
		return "deleted";
	}

	@GetMapping(value = { "/all", "/all/{search}" })
	public List<RequestDto> getEmployees(@PathVariable(required = false) String search) {
		if (search != null) {
			return employeeService.getEmployeesBySearch(search);
		} else {
			return employeeService.getEmployees();
		}

	}

	@PostMapping("/edit")
	public ResponseEntity<String> editEmployee(@RequestParam("image") MultipartFile image,
			@RequestParam("emp") String emp) throws JsonParseException, JsonMappingException, IOException {
		final ObjectMapper mapper = new ObjectMapper();
		final RequestDto request = mapper.readValue(emp, RequestDto.class);
		employeeService.editEmployee(request, image);
		return ResponseEntity.ok().body("edited");

	}

	// @PostMapping("/add")
	// public ResponseEntity<?> addEmployee(@RequestBody RequestDto request) {
	// employeeService.addEmployee(request);
	// return ResponseEntity.ok().body("created");
	//
	// }

	@PostMapping("/add")
	public ResponseEntity<String> addEmployee(@RequestParam("image") MultipartFile image,
			@RequestParam("emp") String emp) throws JsonParseException, JsonMappingException, IOException {
		final ObjectMapper mapper = new ObjectMapper();
		final RequestDto request = mapper.readValue(emp, RequestDto.class);
		employeeService.addEmployee(request, image);
		return ResponseEntity.ok().body("created");

	}

}
