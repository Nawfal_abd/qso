package fr.jrdf.qso.qso.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.jrdf.qso.qso.entities.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

	@Query("select e from Employee e where lower(e.user.firstName) like lower(concat('%', :search, '%')) "
			+ "or lower(e.user.lastName) like lower(concat('%', :search, '%')) "
			+ "or lower(e.entreprise.Name) like lower(concat('%', :search, '%'))")
	List<Employee> FindAllByLastNameAndFirstNameAndEntrprise(@Param("search") String search);

}
