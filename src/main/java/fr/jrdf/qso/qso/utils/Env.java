package fr.jrdf.qso.qso.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Env {

	public static String getUrlImages() {
		try {
			return "http://" + InetAddress.getLocalHost().getHostAddress() + ":8080/images/";
		} catch (final UnknownHostException e) {
			e.printStackTrace();
			return null;
		}
	}
}
