package fr.jrdf.qso.qso.utils;

import static java.nio.file.Paths.get;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Random;

import org.apache.commons.io.FilenameUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class ImageHandler {

	private static String ImageDirectory = "../webapps/images/";

	public static String uploadFile(MultipartFile image) throws IOException {
		Path imageStorage = null;
		String imagename = StringUtils.cleanPath(image.getOriginalFilename());
		final String ext = FilenameUtils.getExtension(imagename);
		imagename = getRadmomString() + "." + ext;
		imageStorage = get(ImageDirectory, imagename).toAbsolutePath().normalize();

		// try (InputStream inputStream = image.getInputStream()) {
		// Files.copy(inputStream, imageStorage,
		// StandardCopyOption.REPLACE_EXISTING);
		// }

		final InputStream inputStream = image.getInputStream();
		Files.copy(inputStream, imageStorage, StandardCopyOption.REPLACE_EXISTING);

		return imagename;
	}

	public static String getRadmomString() {
		final String SALTCHARS = "abcdefghijklmnopqrstuvyz1234567890";
		final StringBuilder salt = new StringBuilder();
		final Random rnd = new Random();
		while (salt.length() < 9) { // length of the random string.
			final int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		final String saltStr = salt.toString();
		return saltStr;
	}

}
