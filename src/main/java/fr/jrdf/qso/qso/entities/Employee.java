package fr.jrdf.qso.qso.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	// @Column(name = "date_entry")
	// private LocalDate dateOfEntry;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@OneToOne(cascade = { CascadeType.ALL })
	@JoinColumn(name = "entreprise_id", referencedColumnName = "id")
	private Entreprise entreprise;

	public Employee() {
		super();
	}

	public Employee(int id, User user, Entreprise entreprise) {
		super();
		this.id = id;
		// this.dateOfEntry = dateOfEntry;
		this.user = user;
		this.entreprise = entreprise;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// public LocalDate getDateOfEntry() {
	// return dateOfEntry;
	// }
	//
	// public void setDateOfEntry(LocalDate dateOfEntry) {
	// this.dateOfEntry = dateOfEntry;
	// }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

}
